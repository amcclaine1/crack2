#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hashed = md5(guess, strlen(guess));
    // Compare the two hashes
    if(strcmp(hash, hashed) == 0)
    {
        return 1;
    } 
    else
    {
        return 0;
    }
    // Free any malloc'd memory
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    //Determing file size for memory allocation
    struct stat info;
    if(stat(filename, &info) == -1)
    {
        printf("Can't find the size.");
        exit(1);
    }
    int filesize = info.st_size;
    
    //alocate memory withh filesize data, store it in a pointer to a character. 
    char * mem = malloc(filesize);
    
    //open the file for reading.
    FILE *src = fopen(filename, "r");
    if(!src)
    {
        printf("Can't open Dictionary file for %s\n", filename);
        exit(1);
    }
    
    // Reads the file, 1 byte at a time, into memalloc "mem", and closes the file.
    fread(mem, 1, filesize, src);
    fclose(src);
    
    //determining how many lines are iin the file.
    int linecount= 0;
    for(int i = 0; i < filesize; i++)
    {
        if(mem[i] == '\n')
        {
            linecount++;
        }
    }
    
    
    char ** data = malloc(linecount * sizeof(char *));
    data[0] = strtok(mem, "\n");
    int i = 1;
    while((data[i] = strtok(NULL, "\n")) != NULL)
    {
        i++;
    }
    //data[strlen(data) - 1] = '\0';
    
    *size = linecount;
    return data;
    fclose(src);
    
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary("rockyou100.txt", &dlen);

    //Finding the size of the Hashes file.
    struct stat info;
    if(stat(argv[1], &info) == -1)
    {
        printf("Can't find the size.");
        exit(1);
    }
    int filesize = info.st_size;
    
    //Opening the hashes file for reading.
    FILE *h = fopen(argv[1], "r");
    if(!h)
    {
        printf("Can't open the hashfile for reading.\n");
        exit(1);
    }

    
    //char * hashes = malloc(filesize);
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops
    printf("%d\n\n",filesize);
    char hash[33];
    while(fgets(hash, 33, h) != NULL)
    {
        for(int j = 0; j < dlen; j++)
        {
            if(tryguess(hash, dict[j]) == 1)
            { 
                printf("Hash: %s |||| Password: %s\n",hash, dict[j]);
            }
            j++;
        } 
    }
}
